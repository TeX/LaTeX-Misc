% Packages
\RequirePackage[check-declarations]{expl3}
\RequirePackage { l3keys2e, fancyhdr, zref-totpages }

% Class definition
\ProvidesExplPackage
    {dutch-letter}
    {2015/07/27}
    {1.2}
    {Simple document class for Dutch letters}

% Options
\keys_define:nn { dutch-letter } {
    adresonder  .bool_set:N = \adresonder,
    unknown     .code:n = 
        \bool_if:NTF \l__keys_no_value_bool
            { \PassOptionsToClass{ \l_keys_key_tl }{ letter } }
            { \PassOptionsToClass{ \l_keys_key_tl=#1 }{ letter } },
}

% Process options
\ProcessKeysOptions{ dutch-letter }

% Load letter class
\LoadClass[a4paper]{letter}

% Change the margins
\RequirePackage{geometry}
\geometry{hmargin=30mm,vmargin=30mm}

% Pagenumber depending on # of pages
\pagestyle{fancy}
\fancyhf{}
\tl_set:Nn \headrulewidth { 0pt }
\cfoot{
    \int_compare:nF { \ztotpages <= 1 } { \thepage/\ztotpages }
}

% Define strings
\tl_const:Nn \deuwkenmerktekst  {Uw~kenmerk:~}
\tl_const:Nn \deonskenmerktekst {Ons~kenmerk:~}
\tl_const:Nn \debetrefttekst    {Betreft:~}
\tl_const:Nn \debijlagetekst    {Bijlage:~}
\tl_const:Nn \decctekst         {Kopie~aan:~}
\tl_const:Nn \depstekst         {P.S.~}

% Define distances
\dim_const:Nn \wrskip {\baselineskip}

% Define whitespace
\cs_new:Npn \witregel        { \\[\wrskip] }
\cs_new:Npn \witregels    #1 { \\[#1\wrskip] }
\cs_new:Npn \witregelskip #1 { \vspace{#1\wrskip} }

% User settable variables
\tl_new:N \deafzender
\tl_new:N \degeadresseerde
\tl_new:N \deplaats
\tl_new:N \dedatum
\tl_new:N \debetreft
\tl_new:N \hetonskenmerk
\tl_new:N \hetuwkenmerk
\tl_new:N \debijlage
\tl_new:N \decc
\tl_new:N \deps

% Default date
\tl_set:Nn \dedatum {\today}

% Commands to set all variables
\cs_new:Npn \afzender       #1  { \tl_set:Nn \deafzender {#1} }
\cs_new:Npn \geadresseerde  #1  { \tl_set:Nn \degeadresseerde {#1} }
\cs_new:Npn \plaats         #1  { \tl_set:Nn \deplaats {#1} }
\cs_new:Npn \datum          #1  { \tl_set:Nn \dedatum {#1} }
\cs_new:Npn \betreft        #1  { \tl_set:Nn \debetreft {#1} }
\cs_new:Npn \onskenmerk     #1  { \tl_set:Nn \hetonskenmerk {#1} }
\cs_new:Npn \uwkenmerk      #1  { \tl_set:Nn \hetuwkenmerk {#1} }
\cs_new:Npn \bijlage        #1  { \tl_set:Nn \debijlage {#1} }
\cs_set:Npn \cc             #1  { \tl_set:Nn \decc {#1} }
\cs_set:Npn \ps             #1  { \tl_set:Nn \deps {#1} }

% Start and end of letter
\cs_new:Npn \aanhef #1  { \briefstart #1 }
\cs_new:Npn \ondertekening  #1  {
    \witregelskip{3}
    \bool_if:NF \adresonder {#1 \witregels{2}}
    \briefeind
}

% Content of end of letter
\tl_const:Nn \briefeind {
    \bool_if:NT     \adresonder {\deafzender \witregel}
    \tl_if_empty:NF \deps       {\depstekst \deps \witregel}
    \tl_if_empty:NF \debijlage  {\debijlagetekst \debijlage \witregel}
    \tl_if_empty:NF \decc       {\decctekst \decc \witregel}
}

% Content of start of letter
\tl_const:Nn \briefstart {
    \bool_if:NF     \adresonder {\deafzender \witregel}
    \degeadresseerde \witregel
    \tl_if_empty:NF \deplaats       {\deplaats,~}\dedatum \witregel
    \tl_if_empty:NF \hetuwkenmerk   {\deuwkenmerktekst \hetuwkenmerk
    \tl_if_empty:NTF \hetonskenmerk {\witregel} {\\} }
    \tl_if_empty:NF \hetonskenmerk  {\deonskenmerktekst \hetonskenmerk\witregel}
    \tl_if_empty:NF \debetreft      {\debetrefttekst \debetreft \witregel}
    \par
}
